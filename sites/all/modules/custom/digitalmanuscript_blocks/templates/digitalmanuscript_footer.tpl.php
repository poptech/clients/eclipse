<div class="footer-image-wrapper" style="text-align=center;">
  <a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/"><img alt="Licence Creative Commons" style="border-width:0" src="https://i.creativecommons.org/l/by-sa/4.0/88x31.png" /></a><br />
<span xmlns:dct="http://purl.org/dc/terms/" property="dct:title" style="margin-top:5px;display: inline-block;">Bastien Sibille</span><br />
Les textes sont mis à disposition selon les termes de la <a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/">licence Creative Commons Attribution -  Partage dans les Mêmes Conditions 4.0 International</a>.

  <div class="user-links-wrapper">
  <?php if($user->uid): ?>
    <a class="footer-link" href="/<?php print $language ?>/user"><?php print t("My account"); ?></a>
    <a class="footer-link" href="/<?php print $language ?>/user/logout"><?php print t("Log out"); ?></a>
  <?php else: ?>
    <a class="footer-link" href="/<?php print $language ?>/user"><?php print t("Log in"); ?></a>
  <?php endif; ?>
  </div>
</div>
