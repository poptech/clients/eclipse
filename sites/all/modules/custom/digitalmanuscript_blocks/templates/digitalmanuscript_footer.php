<div class = "footer-image-wrapper">
  <a href = "http://creativecommons.org/licenses/by-sa/3.0/" target="_blank" class = "image"></a>

  <?php if($user->uid): ?>
    <a class="footer-link" href="/<?php print $language ?>/user"><?php print t("My account"); ?></a>
    <a class="footer-link" href="/<?php print $language ?>/user/logout"><?php print t("Log out"); ?></a>
  <?php else: ?>
    <a class="footer-link" href="/<?php print $language ?>/user"><?php print t("Log in"); ?></a>
  <?php endif; ?>
</div>
