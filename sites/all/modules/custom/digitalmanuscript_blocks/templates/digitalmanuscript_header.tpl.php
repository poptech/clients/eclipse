<div class="header-wrapper">
  <a href = '/<?php print $language ?>/first-page' class="header-item logo <?php print $language; ?>"></a>

  <div class="desktop-menu">
    <div class="header-item">
      <?php foreach ($menus['main_menu'] as $key => $menu): ?>
        <?php print l($menu['title'], $menu['link']); ?>
      <?php endforeach; ?>
       <a href="/<?php print $language ?>/newsletter"><?php print t('Newsletter'); ?></a>
       <a href="/<?php print $language ?>/contact"><?php print t('Leave a message'); ?></a>
    </div>

    <!-- <div class="language-switcher">
      <?php // print $language_switcher; ?>
    </div> -->


  <!-- <div class="header-item user-menu">
      <?php print l("Contact", 'contact'); ?>

      <?php if($user): ?>
        <a href="/<?php print $language ?>/user"><?php print t("My account"); ?></a>

      <?php else: ?>
        <a href="/<?php print $language ?>/user"><?php print t("Log in"); ?></a>
      <?php endif; ?>
    </div> -->
  </div>
  <div class="mobile-menu">
    <div class="menu-toggle">
      <span class="icon" aria-hidden="true"></span>
    </div>
    <div class="mobile-menu-inner">
      <?php if(in_array('administrator', $user->roles) || in_array('admin-client', $user->roles)): ?>
        <a href="/fr/node/add">Ajouter</a>
      <?php endif; ?>
      <a href="/<?php print $language ?>/newsletter"><?php print t('Newsletter'); ?></a>
      <?php foreach ($menus['main_menu'] as $key => $menu): ?>
        <?php print l($menu['title'], $menu['link']); ?>
      <?php endforeach; ?>
      <?php foreach ($menus['user_menu'] as $key => $menu): ?>
        <?php print l($menu['title'], $menu['link']); ?>
      <?php endforeach; ?>

    </div>
  </div>
</div>
