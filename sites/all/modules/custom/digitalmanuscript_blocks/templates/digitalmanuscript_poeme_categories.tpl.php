<div class = "toggle">
  <div class = "toggle-inner">
    <a href = '#block-digitalmanuscript-blocks-poeme-categories' class = "image"></a>
    <a href = '#block-digitalmanuscript-blocks-poeme-categories' class = "text text-open active"><?php print t("Open"); ?></a>
    <a href = '#block-digitalmanuscript-blocks-poeme-categories' class = "text text-close"><?php print t("Close"); ?></a>
  </div>
</div>

<div class="categories-content">
  <div class = "categories-wrapper">
    <?php print $view_poeme_by_category; ?>
    <?php print $view_poeme_by_date; ?>
  </div>
</div>

