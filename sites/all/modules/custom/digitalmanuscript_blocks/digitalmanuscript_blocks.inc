<?php

/**
 * The block for logo.
 */
function digitalmanuscript_blocks_logo() {
  $content = '';

  $content .= '<div class = "logo-wrapper">';
  $content .= '</div>';

  return $content;
}

/**
 * Poeme categories.
 */
function digitalmanuscript_blocks_poeme_categories() {
  global $language;
  global $user;

  if($user->uid) {
    $view = views_get_view('keywords');
    $view->set_display('block_poeme_by_date');
    $view->pre_execute();
    $view->execute();
    $view_poeme_by_date = $view->render();
  }
   else {
    $view_poeme_by_date = '';
  }

  $view = views_get_view('keywords');
  $view->set_display('block_poeme_by_category');
  $view->pre_execute();
  $view->execute();
  $view_poeme_by_category = $view->render();



  return theme('digitalmanuscript_poeme_categories', array('view_poeme_by_date' => $view_poeme_by_date, 'view_poeme_by_category' => $view_poeme_by_category, 'language' => $language->language));
}

/**
 * Site header.
 */
function digitalmanuscript_blocks_header() {
  global $language;
  global $user;
  $main_menu = menu_tree('main-menu');
  $menus['main_menu'] = array();
  foreach ($main_menu as $key => $menu) {
    if(is_numeric($key)) {
      $menus['main_menu'][] = array(
        'title' => $menu['#title'],
        'link' => $menu['#href'],
      );
    }
  }

  $main_menu = menu_tree('user-menu');
  $menus['user_menu'] = array();
  foreach ($main_menu as $key => $menu) {
    if(is_numeric($key)) {
      $menus['user_menu'][] = array(
        'title' => $menu['#title'],
        'link' => $menu['#href'],
      );
    }
  }

  $block = module_invoke('locale', 'block_view', 'language_content');

  return theme('digitalmanuscript_header', array('language' => $language->language, 'user' => $user , 'menus' => $menus, 'language_switcher' => $block['content']));
}

function digitalmanuscript_blocks_footer() {
  global $language;
  global $user;
  return theme('digitalmanuscript_footer', array('language' => $language->language, 'user' => $user));
}
