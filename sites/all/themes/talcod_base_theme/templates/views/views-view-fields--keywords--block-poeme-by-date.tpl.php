
<?php if (!empty($fields['title']->separator)): ?>
  <?php print $fields['title']->separator; ?>
<?php endif; ?>

<?php print $fields['title']->wrapper_prefix; ?>
  <?php print $fields['title']->label_html; ?>

  <div class="<?php print $fields['title']->active; ?>">
  <?php print $fields['title']->content; ?>
  </div>
<?php print $fields['title']->wrapper_suffix; ?>
