(function($) {
  $('document').ready(function() {

    // Add class to vertical tabs
    $("li.vertical-tab-button").each(function() {
      var tabTexte = $(this).children('a').children('strong').text();

      tabTexte = jQuery.trim(tabTexte);
      tabTexte = tabTexte.replace(/[<>^ ()'']+/g, '');
      tabTexte = tabTexte.replace(/[éèÉêè]+/g, 'e');
      tabTexte = tabTexte.replace(/[àâÀ]+/g, 'a');
      tabTexte = tabTexte.replace(/[ô]+/g, 'o');
      tabTexte = tabTexte.replace(/[ù]+/g, 'u');
      tabTexte = tabTexte.toLowerCase();
      $(this).addClass(tabTexte);

    });

    // Add view-empty class to view
    $(".view .view-empty").parent(".view").addClass("empty");
    $(".view.empty").parent(".block").addClass("view-empty");
    $(".view.empty").parent("div").parent("div").parent(".block").addClass("view-empty");


  });// End document ready fonctions




  $(function() {


    $("#block-search-form input.form-text").searchText("Rechercher");

    // Sync start date with end date        
    var activeSync = true;

    $("body.page-node-add input#edit-field-date-und-0-value2-datepicker-popup-0").click(function() {

      activeSync = false;

    });

    $("body.page-node-add-evenement #block-system-main,\n\
body.page-node-add-eventgroup #block-system-main").hover(function() {

      if (activeSync !== false) {

        var dateStart = $("body.page-node-add input#edit-field-date-und-0-value-datepicker-popup-0").val();

        $("body.page-node-add input#edit-field-date-und-0-value2-datepicker-popup-0").val(dateStart);
      }

    });



  });

  jQuery.fn.searchText = function(text) {

    if ($(this).val() === "") {
      $(this).val(text);
    }

    $(this).click(function() {
      var texte = $(this).val();
      if (texte === text) {
        $(this).val("");
      } else {
        $(this).val(texte);
      }
    }).blur(function() {
      var texte = $(this).val();
      if (texte === "") {
        $(this).val(text);
      }
    });

  };


})(jQuery);