(function($) {
  $(document).ready(function() {
    $(".toggle .text").click(function(event) {
      event.preventDefault();
      var _this = $(this);
      $(".categories-content").slideToggle("slow", function() {
        $(".toggle .text").addClass("active");
        _this.removeClass("active");
        $(".toggle .image").toggleClass('toggled');
        console.log($(document).height());
        console.log($('.view-display-id-block_poeme_by_category').height());
        $("html, body").animate({ scrollTop: $(document).height() - $('.categories-content').height()-180 }, 500);
      });
    });

    $(".toggle .image").click(function(event) {
      $(".toggle .text.active").click();
    });

    $(".menu-toggle span").click(function() {
      $(".mobile-menu-inner").slideToggle("fast", function() {
      });
    });

  });
})(jQuery);
