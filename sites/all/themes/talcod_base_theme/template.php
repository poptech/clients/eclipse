<?php

/**
 * Callback to disallow access for non admin user.
 */
function _talcod_base_theme_accesscheck() {
  return user_access('administer site configuration');
}

/**
 * Preprocess variables for the html template.
 */
function talcod_base_theme_preprocess_html(&$vars) {

  $node = menu_get_object();
  if($node) {
    $vars['head_title'] = $node->title;
  }

  $vars['classes_array'][] = css_browser_selector();

  // Add browsercamp variables
  if (module_exists("browscap")) {
    $browser_infos = browscap_get_browser();
    $vars['browser'] = strtolower($browser_infos['browser']);
    $vars['version'] = $browser_infos['majorver'];
  }

  // Add user roles
  if ($vars['user']) {
    $rids = array_keys($vars['user']->roles);
    foreach ($rids as $key => $role) {
      $vars['classes_array'][] = 'role-' . drupal_html_class($role);
    }
  }


  if (user_is_anonymous() && arg(0) != 'user') {

    if (arg(0) === 'civicrm') {
      if ($vars['head_title_array']['title'] === "Accès refusé" || $vars['head_title_array']['title'] === "Access denied") {
        header('Location: /user?destination=' . current_path());
        exit();
      }
    }
    else if ($router_item = menu_get_item(current_path())) {
      if (!$router_item['access']) {
        $_GET['destination'] = '';
        header('Location: /user?destination=' . current_path());
        exit();
      }
    }
  }

  // print_r($_SERVER['REQUEST_URI']);
}

function talcod_base_theme_process_html(&$vars) {

}

function talcod_base_theme_preprocess_page(&$vars) {

}

function talcod_base_theme_process_page(&$vars) {

}

/**
 * Override or insert variables into the node templates.
 */
function talcod_base_theme_preprocess_node(&$vars) {

}

function talcod_base_theme_process_node(&$vars) {
}

function talcod_base_theme_form_alter(&$form, $form_state, $form_id) {

  foreach (node_type_get_types() as $type) {

    if ($form_id === $type->type . '_node_form') {

      $form['options']['sticky']['#access'] = FALSE;
      if (!_talcod_base_theme_accesscheck()) {
        $form['author']['#access'] = FALSE;
        $form['revision_information']['#access'] = FALSE;
      }
    }
  }
}

function talcod_base_theme_views_pre_render(&$view) {
  switch ($view->name) {
    case 'nodehierarchy_pages_list':
      if (user_access('create page content') && user_access('edit all node parents') && user_access('create child nodes')) {
        $view->attachment_before = l(t('Add'), 'node/add/page', array(
          'attributes' => array(
            'class' => array('add-bt'),
            'title' => 'Ajouter une sous-partie',
          ),
          'query' => array(
            'parent' => arg(1),
            'destination' => $_GET['q'],
          ),
            )
        );
      }
    break;

    case 'keywords':
      // keep unique nids.
      $nids = array();
      foreach ($view->result as $key => $result) {
        if(in_array($result->nid, $nids)) {
          unset($view->result[$key]);
        }
        else {
          $nids[] = $result->nid;
        }
      }
    break;
  }
}

function talcod_base_theme_node_view_alter(&$build) {
  unset($build['nodehierarchy_children_links']);
}

/*
function talcod_base_theme_node_view($node, $view_mode = 'full', $langcode = NULL){

    drupal_set_message('<pre>'. print_r($node, true) .'</pre>');

}*/

function talcod_base_theme_preprocess_field(&$variables, $hook) {
}

function talcod_base_theme_preprocess_views_view_unformatted(&$vars) {
  if($vars['view']->name == 'keywords') {
    $nid = arg(1);
    $tid = arg(2);
    $results = $vars['view']->result;
    $current_rows = $vars['rows'];
    $active = FALSE;
    foreach ($current_rows as $result_id => $row) {
      if($results[$result_id]->nid == $nid) {
        $active = TRUE;
        break;
      }

      elseif(isset($results[$result_id]->field_field_poeme_category[0]['raw']['tid']) && $results[$result_id]->field_field_poeme_category[0]['raw']['tid'] == $tid) {
        $active = TRUE;
        break;
      }

      elseif(isset($results[$result_id]->field_field_poeme_date[0]['raw']['tid']) && $results[$result_id]->field_field_poeme_date[0]['raw']['tid'] == $tid) {
        $active = TRUE;
        break;
      }
    }
    $vars['options']['group_class'] = $active ? 'active' : 'inactive';
  }
}

function talcod_base_theme_preprocess_views_view_fields(&$vars) {
  if($vars['view']->name == 'keywords') {
    $nid = arg(1);
    if($nid == $vars['row']->nid) {
      $vars['fields']['title']->active = 'active';
    }

    else {
      $vars['fields']['title']->active = 'inactive';
    }
  }
}

function talcod_base_theme_button($variables) {
  $variables['element']['#attributes']['class'][] = 'btn btn-warning';
  return theme_button($variables);
}

function talcod_base_theme_textfield($variables) {
  $variables['element']['#attributes']['class'][] = 'form-control';
  return theme_textfield($variables);
}

function talcod_base_theme_password($variables) {
  $variables['element']['#attributes']['class'][] = 'form-control';
  return theme_password($variables);
}

function talcod_base_theme_textarea($variables) {
  $variables['element']['#attributes']['class'][] = 'form-control';
  return theme_textarea($variables);
}

function talcod_base_theme_menu_local_tasks($variables) {
  //return theme_menu_local_tasks($variables);
}

function talcod_base_theme_table($variables) {
  $variables['attributes']['class'][] = 'table';
  return theme_table($variables);
}

function talcod_base_theme_preprocess_simplenews_newsletter_footer(&$variables) {
  if($variables['language'] == 'en') {
    $variables['unsubscribe_text'] = "Unsubscribe from this newsletter";
  }

  elseif($variables['language'] == 'fr') {
    $variables['unsubscribe_text'] = "Se désabonner de cette lettre d'information";
  }
}
